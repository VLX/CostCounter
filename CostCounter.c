#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
main(){
	float slpcmp=0,slpho=0,slpmo=0,slpro=0,slpap=0,slpst=0,slpoth=0,slpsum=0; //sleep
	float fdtav=0,fdres=0,fdff=0,fdgbs=0,fdck=0,fdoth=0,fdsum=0;//food
	float trtax=0,trbus=0,trren=0,trmetr=0,trtro=0,trtrai=0,trair=0,trsh=0,trbo=0,trgas=0,troth=0,trsum=0;//transport
	float entbe=0,entcs=0,entwap=0,enthos=0,entic=0,enteve=0,entamp=0,entbar=0,entnic=0,entbep=0,entcas=0,entwas=0,entoth=0,entsum=0;//entertainment
	float shocl=0,shobe=0,shosou=0,shosho=0,shoacc=0,shojew=0,shotp=0,shooth=0,shosum=0;//shopping
	float educit=0,edumu=0,eduars=0,eduli=0,eduoth=0,edusum=0;//education
	float relsp=0,relma=0,relja=0,relpe=0,relyo=0,relsa=0,reloth=0,relsum=0;//relaxation
	float beta=0,beso=0,bepi=0,beha=0,beps=0,beto=0,beoth=0,besum=0;//beauty
	float temp,sum=0;
	char sel1='0',sel2='0';
	FILE *ptr;
 	printf("WELCOME!\n\n");
 	printf("THIS PROGRAMME SUPPORTS ONLY 1 TYPE OF MONEY OF YOUR SELECTION \n SO DO NOT MIX THEIR VALUES, E.G. EUROS WITH DOLLARS\n\n");
 	printf("IF YOU WANT TO REUSE THIS PROGRAMME FOR YOUR NEXT HOLIDAYS\n PLEASE DELETE ALL TXT FILES, THEY ARE IN THE SAME PLACE WITH COSTCOUNTER\n");
 	while((sel1>'9')||(sel1<'9')){
	printf("\nSelect category, insert the number to the left of the word:\n");
	printf("1 SLEEP\n");
	printf("2 FOOD\n");
	printf("3 TRANSPORT\n");
	printf("4 ENTERTAINMENT\n");
	printf("5 SHOPPING\n");
	printf("6 EDUCATION\n");
	printf("7 RELAXATION\n");
	printf("8 BEAUTY\n");
	printf("9 END OF HOLIDAYS\n");
	printf("0 EXIT PROGRAMME\n");
	sel1=getch();
	switch(sel1){	
		case '1':{
				printf("\nSelect subcategory, insert the number to the left of the word:\n\n");
			 printf("SLEEP\n");
		        printf("   1 CAMPING\n");
				printf("   2 HOTEL\n"); 
				printf("   3 MOTEL\n");
				printf("   4 ROOMS\n");
				printf("   5 APARTMENTS\n");
				printf("   6 STUDIOS\n");
				printf("   7 OTHER\n");
				printf("\n   8 GO BACK\n");
				sel2=getch();
				if(sel2<'8')
				printf("Please insert the value\n");
				switch(sel2){
				case '1':{
					if((ptr=fopen("camping.txt","r"))!=0){
					fscanf(ptr,"%f",&slpcmp);
					fclose(ptr);}
					temp=slpcmp;
					scanf("%f",&slpcmp);
					slpcmp=slpcmp+temp;
					ptr=fopen("camping.txt","w");
					fprintf(ptr,"%f",slpcmp);
					fclose(ptr);
					printf("\nThe total cost of camping is %8.2f\n",slpcmp);
					break;
				}
				case '2':{
					if((ptr=fopen("hotel.txt","r"))!=0){
					fscanf(ptr,"%f",&slpho);
					fclose(ptr);}
					temp=slpho;
					scanf("%f",&slpho);
					slpho=slpho+temp;
					ptr=fopen("hotel.txt","w");
					fprintf(ptr,"%f",slpho);
					fclose(ptr);
					printf("\nThe total cost of hotel is %8.2f\n",slpho);
					break;
				}
				case '3':{
					if((ptr=fopen("motel.txt","r"))!=0){
					fscanf(ptr,"%f",&slpmo);
					fclose(ptr);}
					temp=slpmo;
					scanf("%f",&slpmo);
					slpmo=slpmo+temp;
					ptr=fopen("motel.txt","w");
					fprintf(ptr,"%f",slpmo);
					fclose(ptr);
					printf("\nThe total cost of motel is %8.2f\n",slpmo);
					break;
				}
				case '4':{
					if((ptr=fopen("rooms.txt","r"))!=0){
					fscanf(ptr,"%f",&slpro);
					fclose(ptr);}
					temp=slpro;
					scanf("%f",&slpro);
					slpro=slpro+temp;
					ptr=fopen("rooms.txt","w");
					fprintf(ptr,"%f",slpro);
					fclose(ptr);
					printf("\nThe total cost of rooms is %8.2f\n",slpro);
					break;
				}
				case '5':{
					if((ptr=fopen("apartments.txt","r"))!=0){
					fscanf(ptr,"%f",&slpap);
					fclose(ptr);}
					temp=slpap;
					scanf("%f",&slpap);
					slpap=slpap+temp;
					ptr=fopen("apartments.txt","w");
					fprintf(ptr,"%f",slpap);
					fclose(ptr);
					printf("\nThe total cost of apartments is %8.2f\n",slpap);
					break;
				}
				case '6':{
					if((ptr=fopen("studios.txt","r"))!=0){
					fscanf(ptr,"%f",&slpst);
					fclose(ptr);}
					temp=slpst;
					scanf("%f",&slpst);
					slpst=slpst+temp;
					ptr=fopen("studios.txt","w");
					fprintf(ptr,"%f",slpst);
					fclose(ptr);
					printf("\nThe total cost of studios is %8.2f\n",slpst);
					break;
				}
				case '7':{
					if((ptr=fopen("sleep_other.txt","r"))!=0){
					fscanf(ptr,"%f",&slpoth);
					fclose(ptr);}
					temp=slpoth;
					scanf("%f",&slpoth);
					slpoth=slpoth+temp;
					ptr=fopen("sleep_other.txt","w");
					fprintf(ptr,"%f",slpoth);
					fclose(ptr);
					printf("\nThe total cost of other sleep subcategory is %8.2f\n",slpoth);
					break;
				}
				case '8':{
					break;
				}
				default:{
					printf("\nSorry wrong number, please try again\n");
				}
			    }
				break;}	
		case '2':{
				printf("\nSelect subcategory, insert the number to the left of the word:\n\n");
		printf("FOOD\n");
        	printf("   1 TAVERN\n");
        	printf("   2 RESTAURANT\n");
        	printf("   3 FAST FOOD\n");
        	printf("   4 GOODS BY A SUPERMARKET\n");
        	printf("   5 COOKING\n");
        	printf("   6 OTHER\n");
        	printf("\n   7 GO BACK\n");
        	sel2=getch();
        	if(sel2<'7')
			printf("Please insert the value\n");
			 switch(sel2){
        	case '1':{
					if((ptr=fopen("tavern.txt","r"))!=0){
					fscanf(ptr,"%f",&fdtav);
					fclose(ptr);}
					temp=fdtav;
					scanf("%f",&fdtav);
					fdtav=fdtav+temp;
					ptr=fopen("tavern.txt","w");
					fprintf(ptr,"%f",fdtav);
					fclose(ptr);
					printf("\nThe total cost of tavern is %8.2f\n",fdtav);
					break;
				}
			case '2':{
					if((ptr=fopen("restaurant.txt","r"))!=0){
					fscanf(ptr,"%f",&fdres);
					fclose(ptr);}
					temp=fdres;
					scanf("%f",&fdres);
					fdres=fdres+temp;
					ptr=fopen("restaurant.txt","w");
					fprintf(ptr,"%f",fdres);
					fclose(ptr);
					printf("\nThe total cost of restaurant is %8.2f\n",fdres);
					break;
				}
        	case '3':{
						if((ptr=fopen("fast_food.txt","r"))!=0){
					fscanf(ptr,"%f",&fdff);
					fclose(ptr);}
					temp=fdff;
					scanf("%f",&fdff);
					fdff=fdff+temp;
					ptr=fopen("fast_food.txt","w");
					fprintf(ptr,"%f",fdff);
					fclose(ptr);
					printf("\nThe total cost of fast food is %8.2f\n",fdff);
					break;
				}
        	case '4':{
						if((ptr=fopen("goods_by_a_supermarket.txt","r"))!=0){
					fscanf(ptr,"%f",&fdgbs);
					fclose(ptr);}
					temp=fdgbs;
					scanf("%f",&fdgbs);
					fdgbs=fdgbs+temp;
					ptr=fopen("goods_by_a_supermarket.txt","w");
					fprintf(ptr,"%f",fdgbs);
					fclose(ptr);
					printf("\nThe total cost of goods by a supermarket is %8.2f\n",fdgbs);
					break;
				}
        	case '5':{
					if((ptr=fopen("cooking.txt","r"))!=0){
					fscanf(ptr,"%f",&fdck);
					fclose(ptr);}
					temp=fdck;
					scanf("%f",&fdck);
					fdck=fdck+temp;
					ptr=fopen("cooking.txt","w");
					fprintf(ptr,"%f",fdck);
					fclose(ptr);
					printf("\nThe total cost of cooking is %8.2f\n",fdck);
					break;
				}
        	case '6':{
					if((ptr=fopen("food_other.txt","r"))!=0){
					fscanf(ptr,"%f",&fdoth);
					fclose(ptr);}
					temp=fdoth;
					scanf("%f",&fdoth);
					fdoth=fdoth+temp;
					ptr=fopen("food_other.txt","w");
					fprintf(ptr,"%f",fdoth);
					fclose(ptr);
					printf("\nThe total cost of other food subcategory is %8.2f\n",fdoth);
					break;
				}
				case '7':{
					break;
				}
				default:{
					printf("\nSorry wrong number, please try again\n");
				}
		}
			break;
	}
        case '3':{	
			printf("\nSelect subcategory, insert the number/character to the left of the word:\n\n");			
            printf("TRANSPORT\n");
        	printf("   1 TAXI\n");
        	printf("   2 BUS\n");
        	printf("   3 RENTED CAR/MOTORCYCLE\n");
        	printf("   4 METRO\n");
        	printf("   5 TROLLEY\n");
        	printf("   6 TRAIN\n");
        	printf("   7 AIRPLANE\n");
        	printf("   8 SHIP\n");
        	printf("   9 BOAT\n");
        	printf("   a GAS FOR YOUR CAR/MOTORCYCLE\n");
        	printf("   b OTHER\n");
        	printf("\n   c GO BACK\n");
        	sel2=getch();
        	if((sel2<='9')||(sel2=='a')||(sel2=='b'))
        	printf("Please insert the value\n");
			switch(sel2){
        	case '1':{
					if((ptr=fopen("taxi.txt","r"))!=0){
					fscanf(ptr,"%f",&trtax);
					fclose(ptr);}
					temp=trtax;
					scanf("%f",&trtax);
					trtax=trtax+temp;
					ptr=fopen("taxi.txt","w");
					fprintf(ptr,"%f",trtax);
					fclose(ptr);
					printf("\nThe total cost of taxi is %8.2f\n",trtax);
					break;
                 }  
        	case '2':{
					if((ptr=fopen("bus.txt","r"))!=0){
					fscanf(ptr,"%f",&trbus);
					fclose(ptr);}
					temp=trbus;
					scanf("%f",&trbus);
					trbus=trbus+temp;
					ptr=fopen("bus.txt","w");
					fprintf(ptr,"%f",trbus);
					fclose(ptr);
					printf("\nThe total cost of bus is %8.2f\n",trbus);
					break;
                 } 
        	case '3':{
						if((ptr=fopen("rented_car_motorcycle.txt","r"))!=0){
					fscanf(ptr,"%f",&trren);
					fclose(ptr);}
					temp=trren;
					scanf("%f",&trren);
					trren=trren+temp;
					ptr=fopen("rented_car_motorcycle.txt","w");
					fprintf(ptr,"%f",trren);
					fclose(ptr);
					printf("\nThe total cost of rented car/motorcycle is %8.2f\n",trren);
					break;
                 } 
        	case '4':{
					if((ptr=fopen("metro.txt","r"))!=0){
					fscanf(ptr,"%f",&trmetr);
					fclose(ptr);}
					temp=trmetr;
					scanf("%f",&trmetr);
					trmetr=trmetr+temp;
					ptr=fopen("metro.txt","w");
					fprintf(ptr,"%f",trmetr);
					fclose(ptr);
					printf("\nThe total cost of metro is %8.2f\n",trmetr);
					break;
                 } 
        	case '5':{
						if((ptr=fopen("trolley.txt","r"))!=0){
					fscanf(ptr,"%f",&trtro);
					fclose(ptr);}
					temp=trtro;
					scanf("%f",&trtro);
					trtro=trtro+temp;
					ptr=fopen("trolley.txt","w");
					fprintf(ptr,"%f",trtro);
					fclose(ptr);
					printf("\nThe total cost of trolley is %8.2f\n",trtro);
					break;
                 } 
        	case '6':{
						if((ptr=fopen("train.txt","r"))!=0){
					fscanf(ptr,"%f",&trtrai);
					fclose(ptr);}
					temp=trtrai;
					scanf("%f",&trtrai);
					trtrai=trtrai+temp;
					ptr=fopen("train.txt","w");
					fprintf(ptr,"%f",trtrai);
					fclose(ptr);
					printf("\nThe total cost of train is %8.2f\n",trtrai);
					break;
                 } 
        	case '7':{
						if((ptr=fopen("airplane.txt","r"))!=0){
					fscanf(ptr,"%f",&trair);
					fclose(ptr);}
					temp=trair;
					scanf("%f",&trair);
					trair=trair+temp;
					ptr=fopen("airplane.txt","w");
					fprintf(ptr,"%f",trair);
					fclose(ptr);
					printf("\nThe total cost of airplane is %8.2f\n",trair);
					break;
                 } 
        	case '8':{
					if((ptr=fopen("ship.txt","r"))!=0){
					fscanf(ptr,"%f",&trsh);
					fclose(ptr);}
					temp=trsh;
					scanf("%f",&trsh);
					trsh=trsh+temp;
					ptr=fopen("ship.txt","w");
					fprintf(ptr,"%f",trsh);
					fclose(ptr);
					printf("\nThe total cost of ship is %8.2f\n",trsh);
					break;
                 } 
        	case '9':{
						if((ptr=fopen("boat.txt","r"))!=0){
					fscanf(ptr,"%f",&trbo);
					fclose(ptr);}
					temp=trbo;
					scanf("%f",&trbo);
					trbo=trbo+temp;
					ptr=fopen("boat.txt","w");
					fprintf(ptr,"%f",trbo);
					fclose(ptr);
					printf("\nThe total cost of boat is %8.2f\n",trbo);
					break;
                 } 
        	case 'a':{
        		    	if((ptr=fopen("gas_for_your_car_motorcycle.txt","r"))!=0){
        		    fscanf(ptr,"%f",&trgas);
        		    fclose(ptr);}
                    temp=trgas;        		
        			scanf("%f",&trgas);
        			trgas=trgas+temp;
        		    ptr=fopen("gas_for_your_car_motorcycle.txt","w");
					fprintf(ptr,"%f",trgas);
					fclose(ptr);
					printf("\nThe total cost of gas for your car/motorcycle is %8.2f\n",trgas);
					break;
                 } 
        	case 'b':{
				    if((ptr=fopen("transport_other.txt","r"))!=0){
					fscanf(ptr,"%f",&troth);
					fclose(ptr);}
					temp=troth;
					scanf("%f",&troth);
					troth=troth+temp;
					ptr=fopen("transport_other.txt","w");
					fprintf(ptr,"%f",troth);
					fclose(ptr);
					printf("\nThe total cost of other transport subcategory is %8.2f\n",troth);
					break;
                 }
			case 'c':{
					break;
				}
            default:{
	        	printf("\nSorry wrong input, please try again\n");
				}	  
        }
			break;
		}
		case '4':{
				printf("\nSelect subcategory, insert the number/character to the left of the word:\n\n");
			printf("ENTERTAINMENT\n");
			printf("   1 BEACH\n");
			printf("   2 COFFEE SHOP\n");
			printf("   3 WATER PARK\n");
			printf("   4 HOTEL SERVICES\n");
			printf("   5 INTERNET CAFE\n");
			printf("   6 EVENT\n");
			printf("   7 AMUSEMENT PARK\n");
			printf("   8 BAR\n");
			printf("   9 NIGHT CLUB\n");
			printf("   a BEACH PARTIES\n");
			printf("   b CASINO\n");
			printf("   c WATER SPORTS\n");
			printf("   d OTHER\n");
			printf("\n   e GO BACK\n");
			sel2=getch();
			if((sel2<='9')||(sel2=='a')||(sel2=='b')||(sel2=='c')||(sel2=='d'))
        	printf("Please insert the value\n");
			switch(sel2){
				case '1':{
						if((ptr=fopen("beach.txt","r"))!=0){
					fscanf(ptr,"%f",&entbe);
					fclose(ptr);}
					temp=entbe;
					scanf("%f",&entbe);
					entbe=entbe+temp;
					ptr=fopen("beach.txt","w");
					fprintf(ptr,"%f",entbe);
					fclose(ptr);
					printf("\nThe total cost of beach is %8.2f\n",entbe);
					break;
                 }  
                 case '2':{
						if((ptr=fopen("coffee_shop.txt","r"))!=0){
					fscanf(ptr,"%f",&entcs);
					fclose(ptr);}
					temp=entcs;
					scanf("%f",&entcs);
					entcs=entcs+temp;
					ptr=fopen("coffee_shop.txt","w");
					fprintf(ptr,"%f",entcs);
					fclose(ptr);
					printf("\nThe total cost of coffee shop is %8.2f\n",entcs);
					break;
                 }
                 case '3':{
						if((ptr=fopen("water_park.txt","r"))!=0){
					fscanf(ptr,"%f",&entwap);
					fclose(ptr);}
					temp=entwap;
					scanf("%f",&entwap);
					entwap=entwap+temp;
					ptr=fopen("water_park.txt","w");
					fprintf(ptr,"%f",entwap);
					fclose(ptr);
					printf("\nThe total cost of water park is %8.2f\n",entwap);
					break;
                 }
                 case '4':{
						if((ptr=fopen("hotel_service.txt","r"))!=0){
					fscanf(ptr,"%f",&enthos);
					fclose(ptr);}
					temp=enthos;
					scanf("%f",&enthos);
					enthos=enthos+temp;
					ptr=fopen("hotel_service.txt","w");
					fprintf(ptr,"%f",enthos);
					fclose(ptr);
					printf("\nThe total cost of hotel services is %8.2f\n",enthos);
					break;
                 }
                 case '5':{
						if((ptr=fopen("internet_cafe.txt","r"))!=0){
					fscanf(ptr,"%f",&entic);
					fclose(ptr);}
					temp=entic;
					scanf("%f",&entic);
					entic=entic+temp;
					ptr=fopen("internet_cafe.txt","w");
					fprintf(ptr,"%f",entic);
					fclose(ptr);
					printf("\nThe total cost of internet cafe is %8.2f\n",entic);
					break;
                 }
                 case '6':{
						if((ptr=fopen("events.txt","r"))!=0){
					fscanf(ptr,"%f",&enteve);
					fclose(ptr);}
					temp=enteve;
					scanf("%f",&enteve);
					enteve=enteve+temp;
					ptr=fopen("events.txt","w");
					fprintf(ptr,"%f",enteve);
					fclose(ptr);
					printf("\nThe total cost of event is %8.2f\n",enteve);
					break;
                 }
                 case '7':{
					if((ptr=fopen("amusement_park.txt","r"))!=0){
					fscanf(ptr,"%f",&entamp);
					fclose(ptr);}
					temp=entamp;
					scanf("%f",&entamp);
					entamp=entamp+temp;
					ptr=fopen("amusement_park.txt","w");
					fprintf(ptr,"%f",entamp);
					fclose(ptr);
					printf("\nThe total cost of amusement park is %8.2f\n",entamp);
					break;
                 }
                 case '8':{
						if((ptr=fopen("bar.txt","r"))!=0){
					fscanf(ptr,"%f",&entbar);
					fclose(ptr);}
					temp=entbar;
					scanf("%f",&entbar);
					entbar=entbar+temp;
					ptr=fopen("bar.txt","w");
					fprintf(ptr,"%f",entbar);
					fclose(ptr);
					printf("\nThe total cost of bar is %8.2f\n",entbar);
					break;
                 }
                 case '9':{
						if((ptr=fopen("night_clubs.txt","r"))!=0){
					fscanf(ptr,"%f",&entnic);
					fclose(ptr);}
					temp=entnic;
					scanf("%f",&entnic);
					entnic=entnic+temp;
					ptr=fopen("night_clubs.txt","w");
					fprintf(ptr,"%f",entnic);
					fclose(ptr);
					printf("\nThe total cost of night clubs is %8.2f\n",entnic);
					break;
                 }
				case 'a':{
					if((ptr=fopen("beach_parties.txt","r"))!=0){
					fscanf(ptr,"%f",&entbep);
					fclose(ptr);}
					temp=entbep;
					scanf("%f",&entbep);
					entbep=entbep+temp;
					ptr=fopen("beach_parties.txt","w");
					fprintf(ptr,"%f",entbep);
					fclose(ptr);
					printf("\nThe total cost of beach parties is %8.2f\n",entbep);
					break;
                 }
                 case 'b':{
					if((ptr=fopen("casino.txt","r"))!=0){
					fscanf(ptr,"%f",&entcas);
					fclose(ptr);}
					temp=entcas;
					scanf("%f",&entcas);
					entcas=entcas+temp;
					ptr=fopen("casino.txt","w");
					fprintf(ptr,"%f",entcas);
					fclose(ptr);
					printf("\nThe total cost of casino is %8.2f\n",entcas);
					break;
                 }
                 case 'c':{
						if((ptr=fopen("water_sports.txt","r"))!=0){
					fscanf(ptr,"%f",&entwas);
					fclose(ptr);}
					temp=entwas;
					scanf("%f",&entwas);
					entwas=entwas+temp;
					ptr=fopen("water_sports.txt","w");
					fprintf(ptr,"%f",entwas);
					fclose(ptr);
					printf("\nThe total cost of water sports is %8.2f\n",entwas);
					break;
                 }
				case 'd':{
					if((ptr=fopen("entertainment_other.txt","r"))!=0){
					fscanf(ptr,"%f",&entoth);
					fclose(ptr);}
					temp=entoth;
					scanf("%f",&entoth);
					entoth=entoth+temp;
					ptr=fopen("entertainment_other.txt","w");
					fprintf(ptr,"%f",entoth);
					fclose(ptr);
					printf("\nThe total cost of other entertainment subcategory is %8.2f\n",entoth);
					break;
                 }
                 case 'e':{
					break;
				}
                 default:{
	        	printf("\nSorry wrong input, please try again\n");
				}
			}
			break;
		}
		case '5':{
		printf("\nSelect subcategory, insert the number to the left of the word:\n\n");
		printf("SHOPPING\n");
		printf("   1 CLOTHES\n");
		printf("   2 BEACH EQUIPMENTS\n");
		printf("   3 SOUVENIRS\n");
		printf("   4 SHOES\n");
		printf("   5 ACCESSORIES\n");
		printf("   6 JEWLLERIES\n");
		printf("   7 TECHNOLOGY PRODUCTS\n");
		printf("   8 OTHER\n");
		printf("\n   9 GO BACK\n");
		sel2=getch();
		if(sel2<'9')
        	printf("Please insert the value\n");
			switch(sel2){
				case '1':{
					if((ptr=fopen("clothes.txt","r"))!=0){
					fscanf(ptr,"%f",&shocl);
					fclose(ptr);}
					temp=shocl;
					scanf("%f",&shocl);
					shocl=shocl+temp;
					ptr=fopen("clothes.txt","w");
					fprintf(ptr,"%f",shocl);
					fclose(ptr);
					printf("\nThe total cost of clothes is %8.2f\n",shocl);
					break;
                 }
				 case '2':{
					if((ptr=fopen("beach_equipments.txt","r"))!=0){
					fscanf(ptr,"%f",&shobe);
					fclose(ptr);}
					temp=shobe;
					scanf("%f",&shobe);
					shobe=shobe+temp;
					ptr=fopen("beach_equipments.txt","w");
					fprintf(ptr,"%f",shobe);
					fclose(ptr);
					printf("\nThe total cost of beach equipments is %8.2f\n",shobe);
					break;
                 }
				 case '3':{
					if((ptr=fopen("souvenirs.txt","r"))!=0){
					fscanf(ptr,"%f",&shosou);
					fclose(ptr);}
					temp=shosou;
					scanf("%f",&shosou);
					shosou=shosou+temp;
					ptr=fopen("souvenirs.txt","w");
					fprintf(ptr,"%f",shosou);
					fclose(ptr);
					printf("\nThe total cost of souvenirs is %8.2f\n",shosou);
					break;
                 }
				 case '4':{
						if((ptr=fopen("shoes.txt","r"))!=0){
					fscanf(ptr,"%f",&shosho);
					fclose(ptr);}
					temp=shosho;
					scanf("%f",&shosho);
					shosho=shosho+temp;
					ptr=fopen("shoes.txt","w");
					fprintf(ptr,"%f",shosho);
					fclose(ptr);
					printf("\nThe total cost of shoes is %8.2f\n",shosho);
					break;
                 }
				 case '5':{
					if((ptr=fopen("accessories.txt","r"))!=0){
					fscanf(ptr,"%f",&shoacc);
					fclose(ptr);}
					temp=shoacc;
					scanf("%f",&shoacc);
					shoacc=shoacc+temp;
					ptr=fopen("accessories.txt","w");
					fprintf(ptr,"%f",shoacc);
					fclose(ptr);
					printf("\nThe total cost of accessories is %8.2f\n",shoacc);
					break;
                 }
				 case '6':{
					if((ptr=fopen("jewelleries.txt","r"))!=0){
					fscanf(ptr,"%f",&shojew);
					fclose(ptr);}
					temp=shojew;
					scanf("%f",&shojew);
					shojew=shojew+temp;
					ptr=fopen("jewelleries.txt","w");
					fprintf(ptr,"%f",shojew);
					fclose(ptr);
					printf("\nThe total cost of jewelleries is %8.2f\n",shojew);
					break;
                 }
				 case '7':{
					if((ptr=fopen("technology_products.txt","r"))!=0){
					fscanf(ptr,"%f",&shotp);
					fclose(ptr);}
					temp=shotp;
					scanf("%f",&shotp);
					shotp=shotp+temp;
					ptr=fopen("technology_products.txt","w");
					fprintf(ptr,"%f",shotp);
					fclose(ptr);
					printf("\nThe total cost of technology products is %8.2f\n",shotp);
					break;
                 }
				 case '8':{
					if((ptr=fopen("shopping_other.txt","r"))!=0){
					fscanf(ptr,"%f",&shooth);
					fclose(ptr);}
					temp=shooth;
					scanf("%f",&shooth);
					shooth=shooth+temp;
					ptr=fopen("shopping_other.txt","w");
					fprintf(ptr,"%f",shooth);
					fclose(ptr);
					printf("\nThe total cost of other shopping subcategory is %8.2f\n",shooth);
					break;
                 }
                 case '9':{
					break;
				}
				  default:{
	        	printf("\nSorry wrong input, please try again\n");
				}
		}
			break;  
	}
	case '6':{
		printf("\nSelect subcategory, insert the number to the left of the word:\n\n");
		printf("EDUCATION\n");
		printf("   1 CITY TOUR\n");
		printf("   2 MUSEUM\n");
		printf("   3 ARCHEOLOGICAL SITE\n");
		printf("   4 LIBRARY\n");
		printf("   5 OTHER\n");
		printf("\n   6 GO BACK\n");
		sel2=getch();
		if(sel2<'6')
        	printf("Please insert the value\n");
			switch(sel2){
			case '1':{
					if((ptr=fopen("city_tour.txt","r"))!=0){
					fscanf(ptr,"%f",&educit);
					fclose(ptr);}
					temp=educit;
					scanf("%f",&educit);
					educit=educit+temp;
					ptr=fopen("city_tour.txt","w");
					fprintf(ptr,"%f",educit);
					fclose(ptr);
					printf("\nThe total cost of city tour is %8.2f\n",educit);
					break;
                 }
            case '2':{
					if((ptr=fopen("museum.txt","r"))!=0){
					fscanf(ptr,"%f",&edumu);
					fclose(ptr);}
					temp=edumu;
					scanf("%f",&edumu);
					edumu=edumu+temp;
					ptr=fopen("museum.txt","w");
					fprintf(ptr,"%f",edumu);
					fclose(ptr);
					printf("\nThe total cost of museum is %8.2f\n",edumu);
					break;
                 }
			case '3':{
					if((ptr=fopen("archeological_site.txt","r"))!=0){
					fscanf(ptr,"%f",&eduars);
					fclose(ptr);}
					temp=eduars;
					scanf("%f",&eduars);
					eduars=eduars+temp;
					ptr=fopen("archeological_site.txt","w");
					fprintf(ptr,"%f",eduars);
					fclose(ptr);
					printf("\nThe total cost of archeological site is %8.2f\n",eduars);
					break;
                 }
			case '4':{
					if((ptr=fopen("library.txt","r"))!=0){
					fscanf(ptr,"%f",&eduli);
					fclose(ptr);}
					temp=eduli;
					scanf("%f",&eduli);
					eduli=eduli+temp;
					ptr=fopen("library.txt","w");
					fprintf(ptr,"%f",eduli);
					fclose(ptr);
					printf("\nThe total cost of library is %8.2f\n",eduli);
					break;
                 }
			case '5':{
					if((ptr=fopen("education_other.txt","r"))!=0){
					fscanf(ptr,"%f",&eduoth);
					fclose(ptr);}
					temp=eduoth;
					scanf("%f",&eduoth);
					eduoth=eduoth+temp;
					ptr=fopen("education_other.txt","w");
					fprintf(ptr,"%f",eduoth);
					fclose(ptr);
					printf("\nThe total cost of other education subcategory is %8.2f\n",eduoth);
					break;
                 }
                 case '6':{
					break;
				}
				 default:{
	        	printf("\nSorry wrong input, please try again\n");
				}	 	      
			}
		break;
   }
   case '7':{
   	printf("\nSelect subcategory, insert the number to the left of the word:\n\n");
   	printf("RELAXATION\n");
   	printf("   1 SPA\n");
   	printf("   2 MASSAGE\n");
   	printf("   3 JACUZZI\n");
   	printf("   4 PEELING\n");
   	printf("   5 YOGA\n");
   	printf("   6 SAUNA\n");
   	printf("   7 OTHER\n");
   	printf("\n   8 GO BACK\n");
   	sel2=getch();
   	if(sel2<'8')
        	printf("Please insert the value\n");
			switch(sel2){
			case '1':{
					if((ptr=fopen("spa.txt","r"))!=0){
					fscanf(ptr,"%f",&relsp);
					fclose(ptr);}
					temp=relsp;
					scanf("%f",&relsp);
					relsp=relsp+temp;
					ptr=fopen("spa.txt","w");
					fprintf(ptr,"%f",relsp);
					fclose(ptr);
					printf("\nThe total cost of spa is %8.2f\n",relsp);
					break;
                 }
            case '2':{
					if((ptr=fopen("massage.txt","r"))!=0){
					fscanf(ptr,"%f",&relma);
					fclose(ptr);}
					temp=relma;
					scanf("%f",&relma);
					relma=relma+temp;
					ptr=fopen("massage.txt","w");
					fprintf(ptr,"%f",relma);
					fclose(ptr);
					printf("\nThe total cost of massage is %8.2f\n",relma);
					break;
                 }
			case '3':{
					if((ptr=fopen("jacuzzi.txt","r"))!=0){
					fscanf(ptr,"%f",&relja);
					fclose(ptr);}
					temp=relja;
					scanf("%f",&relja);
					relja=relja+temp;
					ptr=fopen("jacuzzi.txt","w");
					fprintf(ptr,"%f",relja);
					fclose(ptr);
					printf("\nThe total cost of jacuzzi is %8.2f\n",relja);
					break;
                 }
			case '4':{
					if((ptr=fopen("peeling.txt","r"))!=0){
					fscanf(ptr,"%f",&relpe);
					fclose(ptr);}
					temp=relpe;
					scanf("%f",&relpe);
					relpe=relpe+temp;
					ptr=fopen("peeling.txt","w");
					fprintf(ptr,"%f",relpe);
					fclose(ptr);
					printf("\nThe total cost of peeling is %8.2f\n",relpe);
					break;
                 }	 	      
			case '5':{
					if((ptr=fopen("yoga.txt","r"))!=0){
					fscanf(ptr,"%f",&relyo);
					fclose(ptr);}
					temp=relyo;
					scanf("%f",&relyo);
					relyo=relyo+temp;
					ptr=fopen("yoga.txt","w");
					fprintf(ptr,"%f",relyo);
					fclose(ptr);
					printf("\nThe total cost of yoga is %8.2f\n",relyo);
					break;
                 }
            case '6':{
					if((ptr=fopen("sauna.txt","r"))!=0){
					fscanf(ptr,"%f",&relsa);
					fclose(ptr);}
					temp=relsa;
					scanf("%f",&relsa);
					relsa=relsa+temp;
					ptr=fopen("sauna.txt","w");
					fprintf(ptr,"%f",relsa);
					fclose(ptr);
					printf("\nThe total cost of sauna is %8.2f\n",relsa);
					break;
                 }
			case '7':{
					if((ptr=fopen("relaxation_other.txt","r"))!=0){
					fscanf(ptr,"%f",&reloth);
					fclose(ptr);}
					temp=reloth;
					scanf("%f",&reloth);
					reloth=reloth+temp;
					ptr=fopen("relaxation_other.txt","w");
					fprintf(ptr,"%f",reloth);
					fclose(ptr);
					printf("\nThe total cost of other relaxation subcategory is %8.2f\n",reloth);
					break;
                 }
			case '8':{
					break;
				}	 	      
			default:{
	        	printf("\nSorry wrong input, please try again\n");
				}	
		    }
   	break;
   }
   case '8':{
   	printf("\nSelect subcategory, insert the number to the left of the word:\n\n");
   	printf("BEAUTY\n");
   	printf("   1 TATTOO\n");
   	printf("   2 SOLARIUM\n");
   	printf("   3 PIERCING\n");
   	printf("   4 HAIRCUT\n");
   	printf("   5 PLASTIC SURGERY\n");
   	printf("   6 TOILETRY\n");
   	printf("   7 OTHER\n");
   	printf("\n   8 GO BACK\n");
   	sel2=getch();
   	if(sel2<'8')
        	printf("Please insert the value\n");
			switch(sel2){
				case '1':{
					if((ptr=fopen("tattoo.txt","r"))!=0){
					fscanf(ptr,"%f",&beta);
					fclose(ptr);}
					temp=beta;
					scanf("%f",&beta);
					beta=beta+temp;
					ptr=fopen("tattoo.txt","w");
					fprintf(ptr,"%f",beta);
					fclose(ptr);
					printf("\nThe total cost of tattoo is %8.2f\n",beta);
					break;
                 }
                 case '2':{
					if((ptr=fopen("solarium.txt","r"))!=0){
					fscanf(ptr,"%f",&beso);
					fclose(ptr);}
					temp=beso;
					scanf("%f",&beso);
					beso=beso+temp;
					ptr=fopen("solarium.txt","w");
					fprintf(ptr,"%f",beso);
					fclose(ptr);
					printf("\nThe total cost of solarium is %8.2f\n",beso);
					break;
                 }
                 case '3':{
					if((ptr=fopen("piercing.txt","r"))!=0){
					fscanf(ptr,"%f",&bepi);
					fclose(ptr);}
					temp=bepi;
					scanf("%f",&bepi);
					bepi=bepi+temp;
					ptr=fopen("piercing.txt","w");
					fprintf(ptr,"%f",bepi);
					fclose(ptr);
					printf("\nThe total cost of piercing is %8.2f\n",bepi);
					break;
                 }
			     case '4':{
					if((ptr=fopen("haircut.txt","r"))!=0){
					fscanf(ptr,"%f",&beha);
					fclose(ptr);}
					temp=beha;
					scanf("%f",&beha);
					beha=beha+temp;
					ptr=fopen("haircut.txt","w");
					fprintf(ptr,"%f",beha);
					fclose(ptr);
					printf("\nThe total cost of haircut is %8.2f\n",beha);
					break;
                 }
                 case '5':{
					if((ptr=fopen("plastic_surgery.txt","r"))!=0){
					fscanf(ptr,"%f",&beps);
					fclose(ptr);}
					temp=beps;
					scanf("%f",&beps);
					beps=beps+temp;
					ptr=fopen("plastic_surgery.txt","w");
					fprintf(ptr,"%f",beps);
					fclose(ptr);
					printf("\nThe total cost of plastic surgery is %8.2f\n",beps);
					break;
                 }
                 case '6':{
					if((ptr=fopen("toiletry.txt","r"))!=0){
					fscanf(ptr,"%f",&beto);
					fclose(ptr);}
					temp=beto;
					scanf("%f",&beto);
					beto=beto+temp;
					ptr=fopen("toiletry.txt","w");
					fprintf(ptr,"%f",beto);
					fclose(ptr);
					printf("\nThe total cost of toiletry is %8.2f\n",beto);
					break;
                 }
                 case '7':{
					if((ptr=fopen("beauty_other.txt","r"))!=0){
					fscanf(ptr,"%f",&beoth);
					fclose(ptr);}
					temp=beoth;
					scanf("%f",&beoth);
					beoth=beoth+temp;
					ptr=fopen("beauty_other.txt","w");
					fprintf(ptr,"%f",beoth);
					fclose(ptr);
					printf("\nThe total cost of other beauty subcategory is %8.2f\n",beoth);
					break;
                 }
                 case '8':{
					break;
				}
			default:{
	        	printf("\nSorry wrong input, please try again\n");
				}	
            }
   	break;
   }
   case '0':{
   	return 0;
   	break;
   }
}
}

if((ptr=fopen("camping.txt","r"))!=0){
fscanf(ptr,"%f",&slpcmp);
fclose(ptr);}
if((ptr=fopen("hotel.txt","r"))!=0){
fscanf(ptr,"%f",&slpho);
fclose(ptr);}
if((ptr=fopen("rooms.txt","r"))!=0){
fscanf(ptr,"%f",&slpro);
fclose(ptr);}
if((ptr=fopen("apartments.txt","r"))!=0){
fscanf(ptr,"%f",&slpap);
fclose(ptr);}
if((ptr=fopen("studios.txt","r"))!=0){
fscanf(ptr,"%f",&slpst);
fclose(ptr);}
if((ptr=fopen("sleep_other.txt","r"))!=0){
fscanf(ptr,"%f",&slpoth);
fclose(ptr);}
if((ptr=fopen("tavern.txt","r"))!=0){
fscanf(ptr,"%f",&fdtav);
fclose(ptr);}
if((ptr=fopen("restaurant.txt","r"))!=0){
fscanf(ptr,"%f",&fdres);
fclose(ptr);}
if((ptr=fopen("fast_food.txt","r"))!=0){
fscanf(ptr,"%f",&fdff);
fclose(ptr);}
if((ptr=fopen("goods_by_a_supermarket.txt","r"))!=0){
fscanf(ptr,"%f",&fdgbs);
fclose(ptr);}
if((ptr=fopen("cooking.txt","r"))!=0){
fscanf(ptr,"%f",&fdck);
fclose(ptr);}
if((ptr=fopen("food_other.txt","r"))!=0){
fscanf(ptr,"%f",&fdoth);
fclose(ptr);}
if((ptr=fopen("taxi.txt","r"))!=0){
fscanf(ptr,"%f",&trtax);
fclose(ptr);}
if((ptr=fopen("bus.txt","r"))!=0){
fscanf(ptr,"%f",&trbus);
fclose(ptr);}
if((ptr=fopen("rented_car_motorcycle.txt","r"))!=0){
fscanf(ptr,"%f",&trren);
fclose(ptr);}
if((ptr=fopen("metro.txt","r"))!=0){
fscanf(ptr,"%f",&trmetr);
fclose(ptr);}
if((ptr=fopen("trolley.txt","r"))!=0){
fscanf(ptr,"%f",&trtro);
fclose(ptr);}
if((ptr=fopen("train.txt","r"))!=0){
fscanf(ptr,"%f",&trtrai);
fclose(ptr);}
if((ptr=fopen("airplane.txt","r"))!=0){
fscanf(ptr,"%f",&trair);
fclose(ptr);}
if((ptr=fopen("ship.txt","r"))!=0){
fscanf(ptr,"%f",&trsh);
fclose(ptr);}
if((ptr=fopen("boat.txt","r"))!=0){
fscanf(ptr,"%f",&trbo);
fclose(ptr);}
if((ptr=fopen("gas_for_your_car_motorcycle.txt","r"))!=0){
fscanf(ptr,"%f",&trgas);
fclose(ptr);}
if((ptr=fopen("transport_other.txt","r"))!=0){
fscanf(ptr,"%f",&troth);
fclose(ptr);}
if((ptr=fopen("beach.txt","r"))!=0){
fscanf(ptr,"%f",&entbe);
fclose(ptr);}
if((ptr=fopen("coffee_shop.txt","r"))!=0){
fscanf(ptr,"%f",&entcs);
fclose(ptr);}
if((ptr=fopen("water_park.txt","r"))!=0){
fscanf(ptr,"%f",&entwap);
fclose(ptr);}
if((ptr=fopen("hotel_service.txt","r"))!=0){
fscanf(ptr,"%f",&enthos);
fclose(ptr);}
if((ptr=fopen("internet_cafe.txt","r"))!=0){
fscanf(ptr,"%f",&entic);
fclose(ptr);}
if((ptr=fopen("events.txt","r"))!=0){
fscanf(ptr,"%f",&enteve);
fclose(ptr);}
if((ptr=fopen("amusement_park.txt","r"))!=0){
fscanf(ptr,"%f",&entamp);
fclose(ptr);}
if((ptr=fopen("bar.txt","r"))!=0){
fscanf(ptr,"%f",&entbar);
fclose(ptr);}
if((ptr=fopen("night_clubs.txt","r"))!=0){
fscanf(ptr,"%f",&entnic);
fclose(ptr);}
if((ptr=fopen("beach_parties.txt","r"))!=0){
fscanf(ptr,"%f",&entbep);
fclose(ptr);}
if((ptr=fopen("casino.txt","r"))!=0){
fscanf(ptr,"%f",&entcas);
fclose(ptr);}
if((ptr=fopen("water_sports.txt","r"))!=0){
fscanf(ptr,"%f",&entwas);
fclose(ptr);}
if((ptr=fopen("entertainment_other.txt","r"))!=0){
fscanf(ptr,"%f",&entoth);
fclose(ptr);}
if((ptr=fopen("clothes.txt","r"))!=0){
fscanf(ptr,"%f",&shocl);
fclose(ptr);}
if((ptr=fopen("beach_equipments.txt","r"))!=0){
fscanf(ptr,"%f",&shobe);
fclose(ptr);}
if((ptr=fopen("souvenirs.txt","r"))!=0){
fscanf(ptr,"%f",&shosou);
fclose(ptr);}
if((ptr=fopen("shoes.txt","r"))!=0){
fscanf(ptr,"%f",&shosho);
fclose(ptr);}
if((ptr=fopen("accessories.txt","r"))!=0){
fscanf(ptr,"%f",&shoacc);
fclose(ptr);}
if((ptr=fopen("jewelleries.txt","r"))!=0){
fscanf(ptr,"%f",&shojew);
fclose(ptr);}
if((ptr=fopen("technology_products.txt","r"))!=0){
fscanf(ptr,"%f",&shotp);
fclose(ptr);}
if((ptr=fopen("shopping_other.txt","r"))!=0){
fscanf(ptr,"%f",&shooth);
fclose(ptr);}
if((ptr=fopen("city_tour.txt","r"))!=0){
fscanf(ptr,"%f",&educit);
fclose(ptr);}
if((ptr=fopen("museum.txt","r"))!=0){
fscanf(ptr,"%f",&edumu);
fclose(ptr);}
if((ptr=fopen("archeological_site.txt","r"))!=0){
fscanf(ptr,"%f",&eduars);
fclose(ptr);}
if((ptr=fopen("library.txt","r"))!=0){
fscanf(ptr,"%f",&eduli);
fclose(ptr);}
if((ptr=fopen("education_other.txt","r"))!=0){
fscanf(ptr,"%f",&eduoth);
fclose(ptr);}
if((ptr=fopen("spa.txt","r"))!=0){
fscanf(ptr,"%f",&relsp);
fclose(ptr);}
if((ptr=fopen("massage.txt","r"))!=0){
fscanf(ptr,"%f",&relma);
fclose(ptr);}
if((ptr=fopen("jacuzzi.txt","r"))!=0){
fscanf(ptr,"%f",&relja);
fclose(ptr);}
if((ptr=fopen("peeling.txt","r"))!=0){
fscanf(ptr,"%f",&relpe);
fclose(ptr);}
if((ptr=fopen("yoga.txt","r"))!=0){
fscanf(ptr,"%f",&relyo);
fclose(ptr);}
if((ptr=fopen("sauna.txt","r"))!=0){
fscanf(ptr,"%f",&relsa);
fclose(ptr);}
if((ptr=fopen("relaxation_other.txt","r"))!=0){
fscanf(ptr,"%f",&reloth);
fclose(ptr);}
if((ptr=fopen("tattoo.txt","r"))!=0){
fscanf(ptr,"%f",&beta);
fclose(ptr);}
if((ptr=fopen("solarium.txt","r"))!=0){
fscanf(ptr,"%f",&beso);
fclose(ptr);}
if((ptr=fopen("piercing.txt","r"))!=0){
fscanf(ptr,"%f",&bepi);
fclose(ptr);}
if((ptr=fopen("haircut.txt","r"))!=0){
fscanf(ptr,"%f",&beha);
fclose(ptr);}
if((ptr=fopen("plastic_surgery.txt","r"))!=0){
fscanf(ptr,"%f",&beps);
fclose(ptr);}
if((ptr=fopen("toiletry.txt","r"))!=0){
fscanf(ptr,"%f",&beto);
fclose(ptr);}
if((ptr=fopen("beauty_other.txt","r"))!=0){
fscanf(ptr,"%f",&beoth);
fclose(ptr);}
slpsum=slpcmp+slpho+slpmo+slpro+slpap+slpst+slpoth;
fdsum=fdtav+fdres+fdff+fdgbs+fdck+fdoth;
trsum=trtax+trbus+trren+trmetr+trtro+trtrai+trair+trsh+trbo+trgas+troth;
entsum=entbe+entcs+entwap+enthos+entic+enteve+entamp+entbar+entnic+entbep+entcas+entwas+entoth;
shosum=shocl+shobe+shosou+shosho+shoacc+shojew+shotp+shooth;
edusum=educit+edumu+eduars+eduli+eduoth;
relsum=relsp+relma+relja+relpe+relyo+relsa+reloth;
besum=beta+beso+bepi+beha+beps+beto+beoth;
sum=slpsum+fdsum+trsum+entsum+shosum+edusum+relsum+besum;
printf("\nThere are the final costs per category:\n\n");
printf("    SLEEP            %8.2f\n",slpsum);
printf("    FOOD             %8.2f\n",fdsum);
printf("    TRANSPORT        %8.2f\n",trsum);
printf("    ENTERTAINMENT    %8.2f\n",entsum);
printf("    SHOPPING         %8.2f\n",shosum);
printf("    EDUCATION        %8.2f\n",edusum);
printf("    RELAXATION       %8.2f\n",relsum);
printf("    BEAUTY           %8.2f\n",besum);
printf("------------------------------\n");
printf("TOTAL                %8.2f\n\n",sum);
printf("I hope you had a good time!\n\n");
printf("Thank You!\n\n");
	system("pause");
}
